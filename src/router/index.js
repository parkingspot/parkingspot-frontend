import Vue from 'vue'
import Router from 'vue-router'
import Parking from '@/components/Parking'
import Login from '@/components/Login'
import LoginRequired from './login-required'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Parking',
      component: Parking,
      beforeEnter: LoginRequired
    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    }
  ],
  mode: 'history'
})
