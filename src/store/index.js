import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
  state: {
    user: null
  },
  mutations: {
    setUser (state, payload) {
      state.user = payload
    }
  },
  actions: {
    login ({commit}, payload) {
      // TODO: fetch the user with Axios
      if (payload.username === 'john' && payload.password === 'john') {
        const user = {
          id: 1,
          username: 'john',
          name: 'John Doe'
        }
        commit('setUser', user)
      }
    }
  },
  getters: {
    user (state) {
      return state.user
    }
  }
})

